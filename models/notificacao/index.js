'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var NotificacaoSchema = new Schema({
	_id: Schema.Types.ObjectId,
	remetente: Number,
	destinatario: Number,
	tipo: { type: String, 'enum': ['notificacao', 'alerta', 'chat'] },
	prioridade: { type: String, 'enum': ['alta',  'media', 'alta', 'nenhuma'] },
	conteudo: [String],
	data: Date
});