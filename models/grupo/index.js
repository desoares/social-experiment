'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var GrupoSchema = new Schema({
	_id: Schema.Types.ObjectId,
	nome: { type: String, unique: true },
	_gestor_id: Number,
	tipo: { type: String, 'enum': ['unidade', 'superintendencia', 'evento'] },
	local: {
		_unidade_id: { 
			type: Number,
			required: false 
		},
		rua: String,
		numero: String,
		complemento: String,
		cidade: String,
		estado: String,
		referencia: String
	},
	membros :[Number]

});

module.exports = exports = mongoose.model('Grupo', GrupoSchema);