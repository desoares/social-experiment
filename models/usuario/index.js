'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var UsuarioSchema = new Schema({
	
	_usuario_id: { type: Number, index: { unique: true }, required : true },
	_unidade_id: { type: Number, required : true },
	amigos: [Number],
	_gruposId: [Schema.Types.ObjectId],
	
	fotos: {
		perfil: { type: String, default: 'perfil.png' },
		capa: { type: String, default: 'capa.png' },
		compartilhadas: [String],
		contagem: Number//o valor tem que ser compartilhadas.length
	}
});

module.exports = exports = mongoose.model('Usuario', UsuarioSchema);