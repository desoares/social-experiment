'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var EventoSchema = new Schema({
	_id: Schema.Types.ObjectId,
	criador: { type: Number, require: true },
	tipo: { type: String, 'enum': ['escala', 'social', 'trabalho'], required: true },
	
	convidar: { 
		usuarios: [Number], 
		_gruposId: [Schema.Types.ObjectId] 
	},
	
	_confimados_id: [Number], 
	local: {
		_unidade_id: { 
			type: Number,
			required: false 
		},
		rua: String,
		numero: String,
		complemento: String,
		cidade: String,
		estado: String,
		referencia: String
	},
	
	datas: { 
		criacao: Date, 
		evento: { 
			type: Date, 
			required: true 
		}, 
		fim: Date 
	},

	horario: { 
		inicio: { 
			type: String, 
			required: true 
		}, 
		fim: String 
	}
});