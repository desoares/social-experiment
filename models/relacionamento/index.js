'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var RelacionamentoSchema = new Schema({
	_id: Schema.Types.ObjectId,
	solicitante: { type: Number},
	solicitado: { type: Number},
	estado: { type: String, 'enum': ['aceita', 'pendente', 'negada', 'oculta', 'bloqueda'] },
	data: Date
});