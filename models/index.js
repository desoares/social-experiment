'use restrict';

var mongoose = require('mongoose'),
	MsSql = require('mssql'),
	config = require('../config'),
	db = config.get('databases:mongo:default:uri'),
	dbSql = config.get('databases:mssql');

mongoose.connect(db, function (err) {
	if (err) { throw err; }

	console.log('Conectado à base de dados', db);

});

var msSql = new MsSql.Connection(dbSql, function (err) {
	if (err) { throw err.message; } 
	
	console.log('Conectado à base de dados MSSQL ', dbSql.server);
});


module.exports = {
	'Evento': require('./evento'),
	'Grupo': require('./grupo'),
	'Notificacao': require('./notificacao'),
	'Relacionamento': require('./relacionamento'),
	'Usuario': require('./usuario')
};