'use strict';

var nconf = require('nconf'),
    path = require('path'),
    env = process.env.NODE_ENV || 'desenvolvimento',
    config = require(path.join(__dirname, env + '.json'));

function setENV() {

	return (process.env.NODE_ENV ? process.env.NODE_ENV : process.env.NODE_ENV = env);
}
setENV();
nconf.env().defaults(config);

module.exports = nconf;