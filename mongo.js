//JSON aceita, bsicamete,dois tipos de entrada, arrays [] e dicionÃ¡rios {}

//mongo shell

help
//auto-complete com o tab
db // -retorna a base de dados atual
mongorestore folder_name //retoaura a base de dados ou coleÃ§Ãµes que tenha sido 
//armazenadas em uma pasta usando-se o comando mongodump 
show dbs//lista as bases de dadoslocais
show collections;//lista as coleÃ§Ãµes na DB atual

db.collection.insert({ field: "value", field_2: "value_2", field_n: "value_n"});

db.collection.find().pretty() retorna os documentna os campos maiores que 1os de forma organizada

db.collection.find({field: { $gt : 10 }}) retorna apenas documentos com campo expecificado maior que 10

db.collection.find({restrissÃµes na busca}, {campos a retornar})

//ex.:
db.users.find({'name': 'nome', 'age': 10, 'score' : { $gt: 5 }}, { '_id': false, 'name': true, email: 1, password: 0})
//0 e false 1 e true sÃ£o as mesmas coisas
//$lt $gt tambÃ©m podem ser sequidos de e de "egual" ou seja $lte (<=) e //$gte (>=)

db.collection.find({field: { $lt : 10 }}) retorna apenas documentos com campo expecificado menor que 10

db.collection.find( field: { $lte: "H", $gte: "B" })// comparativos podem ser aplicados a strings

db.colecion.find({ field : { $nin: [] }})// retorna todos o s documentos excluÃ­dos do array
db.colecion.find({ field : {$exists: true/false}})
db.colecion.find({ field : {$type: 2}})// os tipos sÃ£o definidos por nÃºmeros da especificaÃ§Ã£o do padrÃ£o BSON ver http://bsonspec.org/ 
db.colecion.find({ field : {$regex: "a" }}) //acha documentos em que hÃ¡ letra a
db.colecion.find({ field : {$regex: "e$" }}) //acha documentos que terminam com a letra e
db.colecion.find({ field : {$regex: "^A" }}) // ertorna documentos em que o campo comeÃ§a com A

/*Buscando dentro de arrays:
NÃ£o hÃ¡ uma sintaxe especÃ­cica para busca de Ã­tens dentro de uma array, 
db.collection.find({ field: 'value'}) deve retornar o "value" tando como uma string quanto como uma string pertencente a um array*/

db.collection.find({ field: { $all: ['value-1', 'value-n']}}); //retorna os documentos que possuirem todos as strings listadas em 
//qualquer ordem no campo "field"
db.collection.find({ field : { $in: ['value-1', 'value-2', 'value-n']}})// retorna todos os documentos que possuirem qualquer um 
//dos itens do array

/*Buscando dentro de documentos embutidos:
considerando o seguinte documento:

{
	"_id" : ObjectId("x"),
	"name" : "nome",a 
	"email" : {
		"work" : "nome@work.com",
		"personal" : "nome@personal.com"
	}
}*/

db.collection.find({ email: {work: "nome@work.com", personal: "nome@personal.com"}}); //retorna o documento acima

db.collection.find({ email: { personal: "nome@personal.com", work: "nome@work.com"}}) //nÃ£o retorna o documento acima por nÃ£o conter 
//os Ã­tens do documento na ordem correta

db.collection.find({ email: {work: "nome@work.com"}}}) //nÃ£o retorna o documento acima por nÃ£o conter todos os Ã­tens do documento

//para efetuar uma busca corretamente dentro de um documento embutido:

db.collection.find({ "email.work": "nome@work.com"}}) //retorna o documento acima usando dot notation
b.collection.find( { field : "value" } ).sort({ field2: -1 }).skip(50).limit(20)// retorna vinte documentos pulando os cinquenta 
//a primeiros em ordem descendente do campo field 2

db.collection.count()// retorna apenas a quantidade de resultados da query
//aceita todos os mesmos parÃ¢metros e modificadores que o mÃ©todo find()

db.colection.update({ field: 'value' }, { field: 'newValue', field2: 'newValue', fieldn: 'newValue' });
//substitue o documento resultante do primeiro objeto com os valores do segundo, campos
//nÃ£o inseridos ficarÃ£o vazios

db.collection.update({ field: 'value'}, { $set: { field_x: 'value_x' }});
//altera ou cria o valor apenas dos campos listados no segundo objeto 

db.collection.update({ field: 'value'}, { $inc { field: 10 }});
//incremente o campo com o inteiro que seque

//{
// 	"_id" : 0,
// 	"a" : [
// 		1,
// 		2,
// 		3,
//		4
// 	]
// }

db.collection.update({ _id: 0 }, {$set: { "a.2": 5 }});
//altera o valor (ou cria) do terceiro Ã­tem do array a, retorna o seguinte
//{
// 	"_id" : 0,
// 	"a" : [
// 		1,
// 		2,
// 		5,//alerou o valor de a[2]
//		4
// 	]
// }
db.collection.update({criteria}, {$unset: {field: "value"}});
//exclue o campo "field" e seus respecticvos valores de todos os documentos resultantes do critÃ©rio de busca

db.collection.update({ _id: 0 }, {$push: { a: 6 }});
//insere 1 valor como Ãºltimo Ã­tem do array

db.collection.update({ _id: 0 }, {$pop: { a: 1 }});
//remove o Ãºltimo Ã­tem do array

db.collection.update({ _id: 0 }, {$pop: { a: -1 }});
//remove o primeiro Ã­tem do array

db.collection.update({ _id: 0 }, {$pushAll: { a: 5, 6, 7, 8 }});
//inser vÃ¡rios Ã­tens no final do array

db.collection.update({ _id: 0 }, {$pull: { a: 5}});
//remove um elemento do array em qualquer localizaÃ§Ã£o

db.collection.update({ _id: 0 }, {$pullAll: { a: 5, 6, 7, 8 }});
//remove todos os Ã­tens listados do array

db.collection.update({ _id: 0 }, {$addToSet: { a: 5 }});
//adiciona um Ã­tem ao array somente quando o valor nÃ£o existir 

db.collection.update({ field: 'value' }, {$set: { field: 'value' }}, { upsert: true });
//atualiza ou cria um documento caso o primeiro parÃ¢metro nÃ£o tenha 
//resultado vÃ¡lido. Caso o valor da quary nÃ£o seja um valor vÃ¡lido
//par inserir na base de dados um documento Ã© criado apenas com os
//valores do segundo parÃ¢metro.
//ex:
db.collection.update({ field: { $gt: 50 } }, { $set: { name: 'Willian' }}, { upsert: true });
//este comando retornaria a inserÃ§Ã£de do seguintr documento
// { 
// 	"_id" : ObjectId('234aeab12c2345dfd34990fb00'),
// 	"name" : "Willian"
// }

db.collection.update({ field: 'value' }, {$set: { field: 'value' }}, { multi: true });
//atualiza todos os documentos que retornarem como resultado
//da query do objeto n oprimeiro parÃ¢metro
//caso multi nÃ£o seja expecificado o mongo altera apenas 
//o primeiro resultado da query

db.collection.remove()// usa os mesmos argumentos de find()
//exclui os resultados da query no objeto do primeiro parÃ¢metro
//um a um

db.collection.drop()// exclui uma coleÃ§Ã£o, mais rÃ¡pido que .remove()

//RETORNANDO ERROS:

db.runCommand( { getLastError : 1 });
//pega o Ãºltimo erro caso nÃ£o haja erro na Ãºltima operaÃ§Ã£o 
//retorna um objeto com "err": null, do contrÃ¡rio retorna
//o texto do erro 

//DRIVER DO MONGO PARA NODEJS

mongoimport -d <database> -c <collection> <file>.json
mongoimport --type csv --headerline weather_data.csv -d weather -c data

//EXEMPLO DE CÃ“DIGO NODEJS PARA FINDONE

var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/course', function (err, db) {
	if (err) throw err;

	var query = { 'grade' : 100 };

	db.collection('grades').findOne(query, function (err, doc) {
		if (err) throw err;

		console.dir(doc);
		db.close();
	});
});

//EXEMPLO DE FIND COM toArray()

var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/course', function (err, db) {
	if (err) throw err;

	var query = { 'grade' : 100 };

	db.collection('grades').find(query).toArray(function (err, docs) {
		if (err) throw err;

		console.dir(docs);

		db.close();
	});
});// Neste exemplo a query Ã© executada e um array Ã© criado apÃ³s todos os resultados
// terem sido retornados

//

//EXEMPLO DE FIND USANDO UM CURSOR E EACH

var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/course', function (err, db) {
	if (err) throw err;

	var query = { 'grade' : 100 };

	var cursor = db.collection('grades').find(query);//apenas cria o cursor
	//nÃ£o executa query atÃ© que o .each conecta no mongo uma vez para cada resultado e 
	//imprime no console

	cursor.each(function(err, doc) {
		if (err) throw err;

		if (doc === null) {
			return db.close();
		}

		console.dir(doc.students + " gor a good grade!");
	});
});

// este segundo exemplo Ã© particularmente melhor porque retorna os 
//resultados antes de varrer toda a base, emcasos em que hÃ¡ muitos resultados
//Ã© que as vantagens sÃ£o mais evidentes

//FIELD PROJECTION

var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017', function (err, db) {
	if (err) throw err;

	var query = { 'grade' : 100 };

	var projection = { 'student' : 1, _id : 0 };

	db.collection('grades').find(query, projection).toArray(function (err, docs) {
		if (err) throw err;

		docs.forEach( function (doc) {
			console.dir(doc);
			console.dir(doc.student + ' got a good grade!');
		});
	});
});

//OPERADORES DO OBJETO query
var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/course', function (err, db) {
	if (err) throw err;

	var query = {'student' : 'Joe', 'grade' : { '$gt' : 80, '$lt' : 95 } };

	db.collection('grades').find(query).each(function (err, doc) {
		if (err) throw err;
		
		if (doc === null) {
			return db.close();
		}

		console.dir(doc);
	});

});

//IMPORTANDO UM ARQUIVO JSON
var MongoClient = require('mongodb').MongoClient,
	request = require('request');

MongoClient.connect('mongodb://localhost:27017/course', function (err, db) {
	if (err) throw err;

	request('http://www.reddit.com/r/technology/.json', function (error, response, body) {
		if (!error && response.statusCode === 200) {
			var obj = JSON.parse(body);

			var stories = obj.data.children.map(function (story) {
				return story.data;
			});

			db.collection('reddit').insert(stories, function (err, data) {
				if (err) throw err;

				console.dir(data);

				db.close();
			});
		}
	});

});

//USANDO O OPERADOR $regex NO MÃ‰TODO FIND

var MongoClient = require('mongodb').MongoClient;
MongoClient.connect('mongodb://localhost:27017/course' function (err, db) {
	if (err) throw err;

	
	var query = { 'title' : '$regex': 'NSA'};

	var projection = { 'title' : 1, '_id': 0 };

	db.collection('reddit').find(query, projection).each(function (err));
});


/*
ÃDICES / INDEXES

Ã­ndices em mongodb sÃ£o listas ordenadas de chaves
vocÃª pode indexar mÃºltiplas chaves em uma coleÃ§Ã£o mas a busca deve comeÃ§ar do Ã­ndice mais a esquerda:
Ex.:

indexando as chaves a, b e c, vocÃª pode realizar uma busca  pela indexaÃ§Ã£o de a, a e b ou a, b e c
nÃ£o podendo fazÃª-la por b, c, b e c, ou a e c.

Indices ocupam espaÃ§o em disco e sempre que um novo item Ã© inserido na coleÃ§Ã£o o Ã­ndice tem que ser 
re-indexado, o que leva tempo, por esta razÃ£o devemos indexar apenas as chaves pelas quais serÃ£o 
realizadas a maior parte das buscas.
*/

//ADICIONANDO ÃNDICE A UM CAMPO DE UMA COLEÃ‡ÃƒO

db.collection.ensureIndex({'field': 1, 'field2': -1, ...});

//field Ã© a chave a ser indexada, o nÃºmero poderia ser 1 ou -1, que indica respectivamente
//ordem crescente ou decrescente

db.system.indexes.find() // Lista todos os Ã­ndices da base de dados
db.collection.getIndexes() // Lista todos os Ã­nces da coleÃ§Ã£o
db.collecton.dropIndex({ 'field' : 1 }) //remove o Ã­ndice expecificado 


// ÃNDICE MULTICHAVES / MULTIKEY INDEXES
// Ã‰ quando vocÃª atribui um Ã­ndice a um campo cujo valor Ã© um array
// isso cria um "index point" para cada Ã­tem do array.
//NÃ£o pode ser usado com arrays paralelos, se houver dois Ã­tens indexados em um documento
//vocÃª pode inserir um array em apenas um deles por documento.
// Ex.: uma coleÃ§Ã£o tem as chaves a e b indexadas.
//Ã© possÃ­vel inserir os sequintes valores:
db.collection.insert({a:[1,3,4], b:1});
db.collection.insert({a:1, b:[1,2,3]});
db.collection.insert({a:3, b:1});
//mas nÃ£o Ã© possÃ­vel inserir dois arrays:
db.collection.insert({a:[1,3,4], b:[1,3,5,6]});
//pode indexar qualquer nÃ­vel do documento, nÃ£o restringindo ao primeiro
db.collecton.ensureIndex({ 'field.array' : 1 })
//inserindo um Ã­ndice Ãºnico:
db.collecton.ensureIndex({ 'field' : 1 }, { unique : true })
//para excluir duplicatas na criaÃ§Ã£o de Ã­ndice Ãºnico
db.collecton.ensureIndex({ 'field' : 1 }, { unique : true, dropDups : true })
//ALERTA, NÃƒO HÃ COMO DEFINIR QUAIS DUPLICATASA SERÃƒO EXCLUÃDAS E QUAL SERÃ MANTIDA

//Ãndices esparsos
//serve para indexar campos podem nÃ£o estar preenchidos en todos os documentos da coleÃ§Ã£o mas que
//devem ser unique. Apenas os documentos que tiverem a chave poderam se beneficiar da indexaÃ§Ã£o na busca
db.collecton.ensureIndex({ 'field' : 1 }, { unique : true, sparse : true })
//ALERTA: AS QUERIES ORDENADAS POR UM ÃNDICE ESPARSO PODEM ESCONDER DOCUMENTOS SEM O CAMPO INDEXADO

//Criando Ã­ndices no background                                                                                                        
db.collecton.ensureIndex({ 'field' : 1 }, { unique : true, background : true })
//criar Ã­ndices no background Ã© mais lento mas permite que se escreva na coleÃ§Ã£o, por padrÃ£o
//os Ã­ndices sÃ£o criados no foreground o que bloqueia a escrita no documento
//em produÃ§Ã£o deve-se usar o background ou separar um dos servidores do replicaset para fazer no foreground
//enquanto os demais replicaset servers fazem a escrita
db.collection.insert({a:[1,3,4], b:1}).explain();//explica a query n Ã© nÃºmero de resultados 
//nscanned Ã© o nÃºmero de documentos examinados pela query

db.collection.stats();//exibe estatÃ­sticas da coleÃ§Ã£o como total de espaÃ§o em disco, 
//tamanho dos Ã­ndices e total de documentos
db.collection.totalIndexSize() // retorna o tamanho total de todos os Ã­ndices de uma coleÃ§Ã£o/

//HINT AN INDEX
db.collection.hint({a:1,b:1,c:1})//diz ao cursor quais Ã­ndices usar na query 
//(a Ã© a chave com um Ã­ndice e 1 Ã© cresente, poderia ser -1)
//cado o valor dentro do objeto seja $natural:1 ele usa apenas o Ã­ndice no _id, que obriga a fazer a query
//em todo o documento

//ÃNDICES GEOESPACIAIS
//Para Ã­ndices geoespaciais Ã© necessÃ¡rio haver um campo neste formato
{'field' : [x,y]}//field pode ter qualquer nome e x e y sÃ£o coodenadas de um plano cartesiano
//Na criaÃ§Ã£o do index deve-se usar o sequinte comando:
db.collection.ensureIndex({ 'field': '2d' });// 2d refere-se a bidimenssional
db.collection.ensureIndex({ 'field': '2d', type: 1 });//o type optional refere-se a ordem, neste caso,
//crescente, poderia ser type: -1 
//para executar buscas com este Ã­dice hÃ¡ alguns operadores especiais
db.collection.find({'$near': [x, y]}); // isso ordena do mais prÃ³xino para o mais distantes

//GEOESPACIAIS ESFÃ‰RICOS
//Funciona de foma bem parecida com os geoespaciais no entanto como se trata de uma esfera 
//x e y sÃ£o respectivamente longitude e latitude e se roda um busca com o seguinte comando:
db.runCommand({ geoNear: 'collection', near: [x, y], spherical: true, maxDistance: 1 });
//o valor de maxDistance Ã© em radianos. A funÃ§Ã£o de geoespaciais esfÃ©ricos Ã© basicamente para 
//armazenamento de dados relativos a localizaÃ§Ã£o como o google maps.

//PROFILER
//Profiler Ã© uma ferramenta do mongo que escreve logs no arquivo system.profile
//ele possue trÃªs nÃ­veis por pardÃ£o: 0, 1 e 2, sendo 0 inativo, nÃ£o faz log, 1 registra apenas queries acima de 100 ms
//e 2 registra todas as queries, 0 Ã© o padrÃ£o.
//precisa inicia o mongo com as opÃ§Ãµes abaixo:
mongod --dbpath /path/to/mongo/mongodb --profile 1 --slowms 2
//profile Ã© o nÃ­vel do profiler podendo ser, como mencionado acima, 0, 1 ou 2, slowms Ã© o nÃºmero de milessegundos
//mÃ­nimo para que a query seja registrada no log
//para ver o log use o comando:
db.system.profile.find();

//MONGOTOP
//mostr onde o mongo estÃ¡ gastando o seu tempo
//mongotop <INTERVALO EM SEGUNDOS>
mongotop 3 

//MONGOSTAT
//exibe estatÃ­siticas do mongo um dos mais importantes Ã© 'idx miss %''
//que Ã© o percentual de Ã­ndices fora da memÃ³ria. Ãndices deve caber completamente na memÃ³ria RAM para coneguir-se
//o melhor desempenho em mongodb
mongostat

//SHARDING
//TÃ©cnica para separa um coleÃ§Ã£o grnade em mÃºltiplos servidores mongodb
//criando-se multiplos servidores mongodb e um roteador mongos como  qual a aplicaÃ£o fala
//insrÃ§Ãµes em sharding deve incluir o "shard key" de todos os servidores

//AGGREGATION FRAMEWORK
//O framework de agregaÃ§Ã£o permite fazer contagens de dados na DB, como por exemplo, contar o nÃºmero de 
//produtosde uma determinada marca...


db.products.aggregate(//inÃ­cio do comando com a base de dados a colÃ§Ã£o e a funÃ§Ã£o aggregate
	[//a funÃ§Ã£o recebe um array de ojetos
		{
			$group:{//$group indica que irÃ¡ a grupar os resustados
				"_id":"$manufacturer", //pelo _id dos fabricantes como indicado nesta parte
				//$manufacturer Ã© o campo da coleÃ§Ã£o acrescido de $
				"num_products"://este Ã© um campo criado para armazenar a agregaÃ§Ã£o
				{
					"$sum":1//operador de soma e o valor a ser somada a cada resultado encontrado
				}
			}
		}
	]);

//AGGREGATION PIPELINE

$project //rechape - 1:1 - remodela o documento
$match //filter - n:1 - permite selecionar apenas os documentos que vocÃª quer analizar na agregaÃ§Ã£o
$group //aggregate - n:1
$sort //sort 1:1 - ordena os documentos em um ordem particular
$skip //skips n:1 - pula um determinado nÃºmero de resultados
$limit //limits n:1
$unwind //normalize 1:n // caso haja documentos com arrays ele retorna um documento para cada Ã­tem do array
$out //output 1:1 - permite dar a saida do comando em uma nova coleÃ§Ã£o
//ainda hÃ¡ $redact e $geonear que nÃ£o foram tratados no curso respectivamente permitem que alguns Ã­tens resultantes 
//sejam omitidos por questÃµes de seguranÃ§a seguranÃ§a e permite filtrar geograficamente	

//COMPOUNDING GROUPING

db.products.aggregate(//inÃ­cio do comando com a base de dados a coleÃ§Ã£o e a funÃ§Ã£o aggregate
	[//a funÃ§Ã£o recebe um array de ojetos
		{
			$group:{//$group indica que irÃ¡ a grupar os resustados
				"_id":{
					"manufacturer" : "$manufacturer",
					"category" : "$category"
				}, //pelo _id dos fabricantes como indicado nesta parte
				//$manufacturer Ã© o campo da coleÃ§Ã£o acrescido de $
				"num_products"://este Ã© um campo criado para armazenar a agregaÃ§Ã£o
				{
					"$sum":1//operador de soma e o valor a ser somada a cada resultado encontrado
				}
			}
		}
	]);
//o _id pode receber um documento complexo que precisa apenas ser Ãºnico(unique)

//AGGREGATION EPRESSIONS
//estas expressÃµes sÃ£o de $group
$sum	//soma o 1 ou o valor dado na 
$avg	//tira a mÃ©dia dos valores de uma chave nos mÃºltiplos documentos
$min 	//acha o menor valor para uma chave
$max 	//
$push 	//adiciona qualquer resultado a um array
$addToSet	//adiciona um valor a um array se ele ainda nÃ£o existir
$first 	// retorna o primeiro valor para uma chave sem 'sort' este valor Ã© arbitrÃ¡rio
$last 	// igual a first mas retorna o Ãºltimo

//$SUM
db.products.aggregate([
	{
		$group: {
			_id:{
				maker: "$manufacturer"
			},
			sum_prices:{
				$sum: "$prices"
			}
		}
	}
]);

//$AVG
db.products.aggregate([
	{
		$group: {
			_id: {
				"category": "$category"
			},
			avg_price: {$avg: "$price"}
		}
	}])

//addToSet
//cria um documento com um array criado apartir dos valores de uma determinada chave
db.products.aggregate([
	{
		$group: {
			_id: {
				"maker": "$manufacturer"
			},
			{
				categories: {"$addToSet": "$category"}
			}
		}
	}
])
//este comando retornaria o seguinte:
{_id : { "maker" : "Amazon"}, "categories" : [ "Tablets" ] }
{_id : { "maker" : "Samsung"}, "categories" : [ "Tablets", "Cell Phones" ] }
{_id : { "maker" : "Apple"}, "categories" : [ "Laptops", "Tablets", "Cell Phones" ] }

//$PUSH 
//similar a addToSet mas nÃ£o garante que os Ã­tens apareÃ§am apenas uma vez:
db.products.aggregate([
	{
		$group: {
			_id: {
				"maker": "$manufacturer"
			},
			categories: {"$push": "$category"}
		}
	}
])
//este comando retornaria o seguinte:
{_id : { "maker" : "Amazon"}, "categories" : [ "Tablets", "Tablets" ] }
{_id : { "maker" : "Samsung"}, "categories" : [ "Tablets", "Cell Phones", "Cell Phones", "Cell Phones" ] }
{_id : { "maker" : "Apple"}, "categories" : [ "Laptops", "Tablets", "Tablets", "Tablets", "Cell Phones" , 
"Cell Phones", "Cell Phones" ] }
//ou seja, retorna uma categoria para cada Ã­tem de cada categoria, mesmo quando ela jÃ¡ foi listada

//$MAX
db.products.aggregate([
	{
		$group: {
			_id: {
				"maker": "$manufacturer"
			},
			max_price: {$max: "$price"}
		}
	}
])
//DOUBLE AGGREGATION
//retorna primeiro o agregamento do primeiro objeto e entÃ£o realiza um agregamento no
//documento resultante
db.grades.aggregate([
	{
		$group: {
			_id: {
				class_id: "$class_id",
				studdent_id: "$studdent_id"
			},
			"avarage":{"$avg": "$score"}
		},
		$group: {
			_id: "$_id.class_id", "average": {"$avg": "average"}
		}
	}
])

//$PROJECT
//Ã‰ UM PIPILINE DIFERENTE DE $group
//ele cria uma nova coleÃ§Ã£o que pode ser remodelado
db.products.aggregate([
	{
		$project: {
			_id:0,// o zero indica que nÃ£o queremos o _id
			"maker": {"$toLower": "$manufacturer"},//cria um novo comapo chamado maker
			//$toLower pega o valor de manufaturer e coloca em lowerCase
			"details":{
				"category": "$category",
				"price": {"$mutipli": ["$price": 10]} 
			},
			"item": "$name"
		}
	}
])
//normalmente se usa o $project para filtrar os campos do resultado antes de se agrupar,
//economizando memÃ³ria

//$MATCH
//funciona como um filtro para os documentos resultantes de se agrupar

db.zips.aggregate([
	{
		$match: {
			state: "CA"//nome do campo na coleÃ§Ã£o: valor que deve corresponder
		}
	}
])
//o comando acima retorna todos os resultados com estado CA
db.zips.aggregate([
	{
		$match: {
			state: "CA"
		}

	},
	{
		$group: {
			_id:"$city",
			population: {$sum: "$pop"},
			zip_codes: {$addToSet: "$_id"}
		}
	}
])
//isto aainda pode evoluir paa isto:
db.zips.aggregate([
	{
		$match: {
			state: "CA"
		}

	},
	{
		$group: {
			_id:"$city",
			population: {$sum: "$pop"},
			zip_codes: {$addToSet: "$_id"}
		}
	},
	{
		$project: {
			_id:0,
			"city": "$_id"
			population: 1,
			zip_codes: 1

		}
	}
])

//$SORT
//sort pode ser feito na RAM ou no disco. Existe um limite de 100MB para todos o s pipelines
//por padrÃ£o o de RAM Ã© usado
//a nÃ£o ser que vocÃª permita o uso do disco. Sort pode ser executado antes ou depois de $group
db.zips.aggregate([
	{
		$match: {
			state: "CA"
		}

	},
	{
		$group: {
			_id:"$city",
			population: {$sum: "$pop"},
			zip_codes: {$addToSet: "$_id"}
		}
	},
	{
		$project: {
			_id:0,
			"city": "$_id"
			population: 1,
			zip_codes: 1

		}
	},
	{
		$sort: {population: -1}//nome do campo na coleÃ§Ã£o: asc, desc
	} 
])

//$SKIP & $LIMIT
//no framework de agregaÃ§Ã£o Ã© importante a ordem em que sÃ£o colocados 
//$skip e $limit, por exemplo: se limit for 10 e anterior e $skip for 15, o resultado serÃ¡ os 10
//primeiros documentos da coleÃ§Ã£o, entÃ£o serÃ£o pulados 15, fazendo assim com que a query retorne vazia

db.zips.aggregate([
	{
		$match: {
			state: "CA"
		}

	},
	{
		$group: {
			_id:"$city",
			population: {$sum: "$pop"},
			zip_codes: {$addToSet: "$_id"}
		}
	},
	{
		$project: {
			_id:0,
			"city": "$_id"
			population: 1,
			zip_codes: 1

		}
	},
	{
		$sort: {population: -1}
	},
	{$skip: 10},//sempre nesta ordem primeiro $skip
    {$limit: 5} //depois $limit
])

//$FIRST & $LAST
//Retorna o primeiro ou o Ãºltimo documento resultante de uma agregaÃ§Ã£o
db.zips.aggregate([
	//a primeira parte pega a populaÃ§Ã£o de todas as cidades de todos os estados
	{$group:
		{
			_id: {state: "$state", city: "$city"}
			population: {$sum: "$pop"}
		}
	},
	//ordena por estado alfabeticamente e por populaÃ§Ã£o da maior para a menor
	{$sort:
		{
			"_id.state": 1,
			"population" -1
		}
	},
	//Agrupa por estado e retorna o primeiro de cada estado, ou seja, a maior populaÃ§Ã£o de cada estado
	{$group:
		{
			_id: "$_id.state",
			city: {$first: "_id.city"}
		}
	}
])

//$UNWIND
//Retorna um array desmantelado com um documento para cada Ã­tem que existia anteriormente no array:
//ex.: Um documento como  que segue:
{ 
	a : 1, 
	b : 2, 
	c: [
		'apple', 
		'pear', 
		'orange'
	]}
// $unwind: "$c" retornaria

{ a : 1, b : 2, c : 'apple' }
{ a : 1, b : 2, c : 'pear' }
{ a : 1, b : 2, c : 'orange' }

//USO DE $UNWIND
db.blog.aggregate([
	//unwind by tag:
	{$unwind: "$tags"},
	//agrupa por tag contando cada tag
	{$group: {
		_id: "$tags",
		"count": {$sum: 1}
		}
	},
	//ordenapor popularidade:
	{$sort: {"$count": -1}},
	//mostra os top 10
	{$limit: 10},
	//muda o nome de _id para tag
	{$project: {
		_id: 0,
		"tag": "$_id",
		"count": 1 
		}
	}
])

//DOUBLE $UNWIND
db.inventary.aggregate([
	{$unwind: "$sizes"},
	{$unwind: "$colors"},
	{$group:
		{
			_id: { "size": "$sizes", "color": "$colors"},
			"count": {$sum: 1}
		}
	}
])
//REVERTENDO $UNWIND
//Ã‰ POSSÃVEL REVERTER UM $UNWIND USANDO $addToSet ou $push, caso o Ã­tem nÃ£o seja "unique" 
//no array deve-se usar $push
db.inventory.aggregate([
	{$unwind: "$sizes"},
	{$unwind: "$colors"},
	//cria o aray de colors
	{$group
		"_id": { "name": "name", size: "$sizes"}
		"colors": {$push: "$colors"}
	},
	//cria o array de siszes
	{$group
		{
			"_id": {"name": "_id.name", "colors": "$colors"},
			"sizes": "$_id.size"
		}
	},
	//reformata no formato original:
	{$project: 
		{
			_id: 0,
			name: "$_id.name",
			sizes: 1,
			colors: "$_id.colors"
		}
	}
])
//exemplo usando-se addToSet (sÃ³ funciona caso cada item seja "unique" no array)
db.inventory.aggregate([
	{$unwind: "$sizes"},
	{$unwind: "$colors"},
	//cria o aray de colors
	{$group
		"_id": { "name": "name"}
		"sizes": {$addToSet: "$sizes"}
		"colors": {$addToSet: "$colors"}
	}
])
/*
APPLICATION ENGENERING
Quando se usa replicaÃ§Ã£o em Mongo deve sempre haver, no mÃ­nimo, 3 nÃ³s.
Os tipos de nÃ³s sÃ£o:
	Regular - pode ser primÃ¡rio ou secundÃ¡rio
	Arbiter - apenas pode votar para eleger o primÃ¡rio, nÃ£o armazena dados nem pode ser primÃ¡rio
	Delayed - grava os dados com atraso em relaÃ§Ã£o ao nÃ³ primÃ¡rio prioridade 0, nÃ£o pode ser primÃ¡rio
	Hidden - tambÃ©m tem prioridade ajustada para zero, o que quer dizer que nÃ£o pode ser primÃ¡rio

Todos os nÃ³s pode votar para eleger um nÃ³ primÃ¡rio caso o primÃ¡rio caia.

//ConsistÃªncia de Escrita

Usando-se ReplicaSet vocÃª pode escrever apenas no nÃ³ primÃ¡rio. Por padrÃ£o vocÃª tambÃ©m sÃ³ pode ler do nÃ³ primÃ¡rio,
porÃ©m, vocÃª pode configurar para ler dos nÃ³s secundÃ¡rios. Neste caso Ã© possÃ­vel que vocÃª leia dados obsoletos 
jÃ¡ que hÃ¡ um atraso na atualizaÃ§Ã£o dos nÃ³s secundÃ¡rios. NÃ£o hÃ¡ garantia no tempo de atraso porque a replicaÃ§Ã£o 
dos dados Ã© assÃ­ncrona.
Mantendo-se o padÃ£o do Mongodb a escrita Ã© fortemente consistente. Caso o nÃ³ primÃ¡rio caia 
nÃ£o Ã© possÃ­vel escrever na base de dados atÃ© que a eleiÃ§Ã£o de um novo nÃ³ primÃ¡rio.

*/

//Iniciando o replicaset
//primeiro vocÃª deve criar as pastas onde os dados serÃ£o armazenados
//mkdir -p /data/rs1 /data/rs2 /data/rs3
///entÃ£o criamos os precessos para cada um deles
mongod --replSet rs1 --logpath "1.log" --dbpath data/rs --port 27017 --fork
mongod --replSet rs1 --logpath "2.log" --dbpath data/rs --port 27018 --fork
mongod --replSet rs1 --logpath "3.log" --dbpath data/rs --port 27019 --fork
//cada um deles tem que ser aberto em uma porta diferente

config = { _id: "m101", members:[
          { _id : 0, host : "localhost:27017"},
          { _id : 1, host : "localhost:27018"},
          { _id : 2, host : "localhost:27019"} ]
};

rs.initiate(config);
rs.status();//mostra o estado da replicaSet

//implicaÃ§Ãµes do desenvolvedor com relaÃ§Ã£o ao uso de ReplicaSet

/*
lista de seeds - o driver de estar ciente d epelomenos um membro do replicaSet
perocupaÃ§Ãµes de escrita - w, j e w-timeout
preferÃªncia de leitura - vai ler do primÃ¡rio apenas ou dos sedundÃ¡rio tambÃ©m
eerros podem acontecer - Ã© preciso verificar exeÃ§Ãµes quando vocÃª escreve e lÃª para ter certeza 
de que vocÃª entende a aplicaÃ§Ã£o e quais dados Ã£o consistentes

*/

rs.isMaster();//diz se o nÃ³ atual Ã© master ou secundÃ¡rio
rs.slaveOk();//permite ler apartir de um nÃ³ secundÃ¡rio

/*
HÃ¡ uma coleÃ§Ã£o chamada oplog.rs que serve para sincronizar os nÃ³s secundÃ¡rios do rs com o nÃ³ primÃ¡rio.
Os secundÃ¡rios buscam nesta coleÃ§Ã£o e comparam com suas prÃ³prias oplog.rs e copiam os dados nÃ£o atualizados.
A criaÃ§Ã£o de Ã­ndices tambÃ©m aparece no oplog
*/

//Failover e Rollback

/*
Rollbacks acontecerÃ£o sempre que o nÃ³ primÃ¡rio cair (failover) e contiver escritas que nÃ£o estava nos servidores secundÃ¡rios. 
Quando o antigo nÃ³ primÃ¡rio voltar como secundÃ¡rio ele criarÃ¡ um arquivo com estes dados que poderÃ£o ser inseridos 
manualmente no novo nÃ³ primÃ¡rio.
Para evitar este cenÃ¡rio deve-se configurar o conjunto do ReplicaSet para esperar que a maioria dos nÃ³s tenha os dados 
configurando o paraÃ¢metro w=1

Fail over dentro do driver de nodejs:
No driver do nodejs Ã© possÃ­vel indicar apenas um no do replicaSet que a aplicaÃ§Ã£o serÃ¡ capaz de utilizar corretamente
o conjunto, porÃ©m caso o nÃ³ indicado ao driver do nodejs esteja "off" a aplicaÃ§Ã£o nÃ£o saberÃ¡ onde conectar-se, por isso Ã© 
recomendavel que se indique todos os nÃ³s.
Qaundo ocorre uma falha no nÃ³ primÃ¡rio o driver do node cria um buffer para armazenas todas as escritas atÃ© que seja
eleito um novo nÃ³ primÃ¡rio.

ConfiguraÃ§Ã£o de Escrita Write Concerne
Ã‰ uma maneira de especificar o comportmento das escritas no mongodb.

	w : 1 - padrÃ£o -> envia a escrita para o nÃ³ primÃ¡rio e notifica o usuÃ¡rio assim que o primÃ¡rio toma conhecimento 
		deste sucesso.
	w : 0 - o driver notifica  sucesso assim que a escrita Ã© enviada sem receber resposta

	w : 2 - somente notifica sucesso apos o nÃ³ primÃ¡rio e, no mÃ­nimo, um nÃ³ secundÃ¡rio informam sucesso. Os demais 
	nÃºmeros indicam quantos nÃ³s devem responder sucesso antes de notificar o usuÃ¡rio.

	w : 'j' - retorna sucesso apÃ³s escrever no diÃ¡rio do nÃ³ primÃ¡rio o que permite recuperaÃ§Ã£o em caso de 
	fail over uma vez que garante que os dados estejam no disco quando notifica o sucesso.

	w : 'majority' - retorna sucesso quando a maioria dos nÃ³s reportar o sucesso da escrita

	w : 1, j : 1 - alÃ© de esperar confirmaÃ§Ã£o do nÃ³ primÃ¡rio espera a escrita no diÃ¡rio do nÃ³ primÃ¡rio

*/

//PREFERÃŠNCIAS DE LEITURA
/*
Por padrÃ£o as leituras sempre irÃ£o para o nÃ³ primÃ¡rio,
no entanto Ã© possÃ­vel configurar as leituras para:
	- primary 
	- secondary
	- prefered (sÃ³ nÃ£o lerÃ¡ o nÃ³ preferencial quando nÃ£o for possÃ­vel lÃª-lo, primaruPrefered, secondaryPrefered)
	- nearest

Pode ainda ser tag set, nÃ£o aboradado no curso
Tag Sets

Tag sets allow you to target read operations to specific members of a replica set.

Custom read preferences and write concerns evaluate tags sets in different ways. Read preferences consider the value of a tag when selecting a member to read from. Write concerns ignore the value of a tag to when selecting a member, except to consider whether or not the value is unique.

You can specify tag sets with the following read preference modes:

primaryPreferred
secondary
secondaryPreferred
nearest
Tags are not compatible with mode primary and, in general, only apply when selecting a secondary member of a set for a read operation. However, the nearest read mode, when combined with a tag set, selects the matching member with the lowest network latency. This member may be a primary or secondary.

extraÃ­do de http://docs.mongodb.org/manual/core/read-preference/
*/

//PARANDO O SERVIDOR

/*
Para parar o servidor primeiro voce deve se conectar Ã  coleÃ§Ã£o admin usando o comando:
>use admin
depois executando o comando:
>db.shutdownServer();
*/

//ERROS DE REDE

/*
Ainda que vocÃª possa setar a escrita para w=1, j=1 Ã© possÃ­vel que escritas bem sucedidas retornem erros em 
virtude de falhas de conexÃ£o
*/

//SHARDING

/*
sharding Ã© a maneira od mongodb de escalonar. Permite colocar uma coleÃ§Ã£o em mÃºtiplos servidores
	 ___     ___ 	 ___	 ___	 ___
	|   |   |	|   |   |	|	|	|	|
	|___|	|___|   |___|	|___|	|___|
	 S1		 S2      S3		 S4		 S5
      |______|_______|_______|________|
                     |
	  /\	       mongos
	 _||_           _|_
	/()()\         /   \
   |  rs  |       | App | 
    \_()_/         \___/

Cada shard Ã©, normalmente, um conjunto de ReplicaSet. Quem faz a comunicaÃ§Ã£o entre o mongodb e 
a aplicaÃ§Ã£o Ã© o mongos. Ele funciona com "RANGE BASED" e o conceito de shard key.
Em uma coleÃ§Ã£o pedidos, pedido_id eria a "shard key". Informando-se a shard key para aplicaÃ§Ã£o permite que uma query 
busque os dados diretamente na shard correta, o nÃ£o uso envia a query para todos as shards.
Se vocÃª utilizar um ambiente com shard vocÃª deverÃ¡ incluir a shard key em todos os inserts. 
Pode-se fazer sharding em toda a DB ou apenas em uma coleÃ§Ã£o, ou coleÃ§Ãµes que vocÃª quer. Ã‰ possÃ­vel haver mais de um 
driver mongos, funcionando de forma bastandte similar ao replicaSet.
*/

// Building a Sharded Environment.


# Andrew Erlichson
# 10gen
# script to start a sharded environment on localhost

# clean everything up
echo "killing mongod and mongos"
killall mongod
killall monogs
echo "removing data files"
rm -rf /data/config
rm -rf /data/shard*


# start a replica set and tell it that it will be a shord0
mkdir -p /data/shard0/rs0 /data/shard0/rs1 /data/shard0/rs2
mongod --replSet s0 --logpath "s0-r0.log" --dbpath /data/shard0/rs0 --port 37017 --fork --shardsvr --smallfiles
mongod --replSet s0 --logpath "s0-r1.log" --dbpath /data/shard0/rs1 --port 37018 --fork --shardsvr --smallfiles
mongod --replSet s0 --logpath "s0-r2.log" --dbpath /data/shard0/rs2 --port 37019 --fork --shardsvr --smallfiles

sleep 5
# connect to one server and initiate the set
mongo --port 37017 << 'EOF'
config = { _id: "s0", members:[
          { _id : 0, host : "localhost:37017" },
          { _id : 1, host : "localhost:37018" },
          { _id : 2, host : "localhost:37019" }]};
rs.initiate(config)
EOF

# start a replicate set and tell it that it will be a shard1
mkdir -p /data/shard1/rs0 /data/shard1/rs1 /data/shard1/rs2
mongod --replSet s1 --logpath "s1-r0.log" --dbpath /data/shard1/rs0 --port 47017 --fork --shardsvr --smallfiles
mongod --replSet s1 --logpath "s1-r1.log" --dbpath /data/shard1/rs1 --port 47018 --fork --shardsvr --smallfiles
mongod --replSet s1 --logpath "s1-r2.log" --dbpath /data/shard1/rs2 --port 47019 --fork --shardsvr --smallfiles

sleep 5

mongo --port 47017 << 'EOF'
config = { _id: "s1", members:[
          { _id : 0, host : "localhost:47017" },
          { _id : 1, host : "localhost:47018" },
          { _id : 2, host : "localhost:47019" }]};
rs.initiate(config)
EOF

# start a replicate set and tell it that it will be a shard2
mkdir -p /data/shard2/rs0 /data/shard2/rs1 /data/shard2/rs2
mongod --replSet s2 --logpath "s2-r0.log" --dbpath /data/shard2/rs0 --port 57017 --fork --shardsvr --smallfiles
mongod --replSet s2 --logpath "s2-r1.log" --dbpath /data/shard2/rs1 --port 57018 --fork --shardsvr --smallfiles
mongod --replSet s2 --logpath "s2-r2.log" --dbpath /data/shard2/rs2 --port 57019 --fork --shardsvr --smallfiles

sleep 5

mongo --port 57017 << 'EOF'
config = { _id: "s2", members:[
          { _id : 0, host : "localhost:57017" },
          { _id : 1, host : "localhost:57018" },
          { _id : 2, host : "localhost:57019" }]};
rs.initiate(config)
EOF


# now start 3 config servers
mkdir -p /data/config/config-a /data/config/config-b /data/config/config-c 
mongod --logpath "cfg-a.log" --dbpath /data/config/config-a --port 57040 --fork --configsvr --smallfiles
mongod --logpath "cfg-b.log" --dbpath /data/config/config-b --port 57041 --fork --configsvr --smallfiles
mongod --logpath "cfg-c.log" --dbpath /data/config/config-c --port 57042 --fork --configsvr --smallfiles


# now start the mongos on a standard port
mongos --logpath "mongos-1.log" --configdb localhost:57040,localhost:57041,localhost:57042 --fork
echo "Waiting 60 seconds for the replica sets to fully come online"
sleep 60
echo "Connnecting to mongos and enabling sharding"

# add shards and enable sharding on the test db
mongo
db.adminCommand( { addShard : "s0/"+"localhost:37017" } );
db.adminCommand( { addShard : "s1/"+"localhost:47017" } );
db.adminCommand( { addShard : "s2/"+"localhost:57017" } );
db.adminCommand({enableSharding: "test"})
db.adminCommand({shardCollection: "test.grades", key: {student_id:1}});

//IMPLICAÃ‡Ã•ES DE SHARDING
/*
-Todo documento deve ter a shard key
-A shard key Ã© imutÃ¡vel, uma vez colocada em um documento nÃ£o pode ser alterada
-Ã‰ preciso ter um Ã­ndice que comece com a shard key, o segundo Ã­tem nÃ£o pode ser multi-key
-Num update a shard key deve ser especificada ou o oupdate deve ser multi
ex.:*/
db.collection.update({ field: 'value' }, {$set: { field: 'value' }}, { multi: true });
/*
-Sempre que uma query for feita sem shard key serÃ¡ enviada a todos os nÃ³s
-Sem Ã­ndices 'unique' exceto que comecem pela shard key
*/