'use restrict';

module.exports = function (app) {
var io = require('socket.io')(app.server),
	numero = 0;

	io.on('connection', function (socket) {
		numero += 1;
		socket.emit('news', 'SPTC Social. ' + numero + ' conectados no momento.');
		console.log('Socket.IO emitindo no arquvo /sockets/index.js --/sockets/conexao/index.js');
		socket.broadcast.emit('news', 'SPTC Social. ' + numero + ' conectados no momento.');
		socket.on('disconnect', function () {
			numero -= 1;
			socket.broadcast.emit('news', 'SPTC Social. ' + numero + ' conectados no momento.');
		});
	});
};