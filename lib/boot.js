'use strict';

var express = require('express'),
	mongoose = require('mongoose'),
	bodyParser = require('body-parser'),
	models = require('../models'),
	config = require('../config');

var app = express();

app.use(bodyParser.json({
  limit: '20480kb'
}));
app.use(bodyParser.urlencoded({
  limit: '20480kb',
  extended: true
}));
app.routes = require('../routes')(app);

module.exports = app;