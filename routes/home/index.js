'use strict';

var mongoose = require('mongoose'),
	Usuario = mongoose.model('Usuario'),
	fs = require('fs');

module.exports.index = function (req, res) {
  fs.readFile('/home/fabio.fds/Projetos/sptc/client/index.php', function (err, data) {
  	if (err) { throw err; }

	  res.writeHead(200);

    res.end(data);	
  });
};

module.exports.teste = function (req, res) {

	var query = { _usuario_id: req.params.id };
	Usuario.findOne(query, function (err, doc) {
		if (err) { 
			return res.status(500).json({ error: err.message });
		}

		if (!doc) {
			return res.status(404).json({ error: "Ususário não encontrado" });
		}
		//console.log(req.params.id);
		res.status(200).json(doc);
	});
};