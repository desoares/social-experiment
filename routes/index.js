'use strict';

var models = require('../models'),
	home = require('./home'),
	login = require('./login');

function routes(app) {
	app.get('/', home.index);
	app.get('/user/:id', home.teste);
	app.post('/login', login.autenticar);
}

module.exports = exports = routes;