'use strict';

var express = require('express'),
	http = require('http'),
	//io = require('socket.io'),
	config = require('./config'),
	routes  = require('./routes'),
	app = require('./lib/boot.js'),
	soquetes = require('./sockets');

//app.models = require('./models');

app.server = http.createServer(app).listen(config.get('port'), function () {
	console.log('');
	console.log('   SSSSSS     PPPPPPPPP    TTTTTTTTTTTTTT     CCCCCC');
	console.log(' SSSSSSSSSS   PPPPPPPPPP   TTTTTTTTTTTTTT   CCCCCCCCC');
	console.log(' SSSS  SSSS   PPPP  PPPP        TTTT        CCCC  CCCC');
	console.log(' SSSS         PPPP  PPPP        TTTT        CCCC');
	console.log('   SSSSSS     PPPPPPPPPP        TTTT        CCCC');
	console.log('       SSSS   PPPPPPPPP         TTTT        CCCC');
	console.log(' SSSS  SSSS   PPPP              TTTT        CCCC  CCCC');
	console.log(' SSSSSSSSSS   PPPP              TTTT        CCCCCCCCC');
	console.log('   SSSSSS     PPPP              TTTT          CCCCCC');
	console.log('');
	console.log('Módulo Social. Escutando na porta', config.get('port') + '.', 
	'Rodando no ambiente:', process.env.NODE_ENV + '.');
});

soquetes.conexao(app);
//ms sql 172.17.0.149